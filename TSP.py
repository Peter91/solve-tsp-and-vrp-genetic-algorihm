import random
 
"""Class gathering the necessary elements of the representation of
    optimization problems to be solved by a genetic algorithm. More
    precisely, these elements are:
   - genes: list of the genes used in the genotype of the individuals
   - individuals_length: length of the chromosomes
   - decode: function that transforms the genotype into the phenotype
   - fitness: function that evaluates the individuals, to be optimized
   - mutation: mutates a chromosome 
   - crossover: given a pair of chromosomes performs crossover on
    them"""

class Problem_Genetic(object):
   
    def __init__(self,genes,individuals_length,decode,fitness):
        self.genes=genes
        self.individuals_length=individuals_length
        self.decode=decode
        self.fitness=fitness

    def mutation(self,chrom,prob):
        cp_chrom= []
        cp_chrom.extend(chrom) 
        flag=True
        while flag:
            if random.random() < prob :
                aux1= random.randrange(len(chrom))
                aux2= random.randrange(len(chrom))
                if aux1 != aux2 :
                    val1= chrom[aux1]
                    val2= chrom[aux2]
                    cp_chrom[aux1]=val2
                    cp_chrom[aux2]=val1
                    flag= False
               
        return cp_chrom

    def crossover(self,c1,c2):
        
        pos=random.randrange(len(c1))               
        kid1=c1[:pos] 
        kid2=c2[:pos] 
        
        for i,j in zip(c1[pos:],c2[pos:]):
            if i not in kid2:
                kid2.append(i)
            if j not in kid1:
                kid1.append(j)

        if len(kid1)& len(kid2) is not 8:            
            for i,j in zip(c1,c2):
                if i not in kid2:
                    kid2.append(i)
                if j not in kid1:
                    kid1.append(j)
                  
           
        return [kid1,kid2]

"""Este metodo da la distancia del recorrido"""
def decimal_to_distancia_ciudad(x):
    distanciaCiudades={'0-1': 93, '0-2': 212, '0-3': 300, '0-4': 250, '0-5': 425, '0-6': 350, '0-7':590,
                       '1-2': 124, '1-3': 206, '1-4': 139, '1-5': 340, '1-6': 260, '1-7': 500,
                       '2-3': 236, '2-4': 263, '2-5': 361, '2-6': 454, '2-7': 523,
                       '3-4': 157, '3-5': 125, '3-6': 218, '3-7': 287,
                       '4-5': 202, '4-6': 117, '4-7': 364,
                       '5-6': 93, '5-7': 162,
                       '6-7': 255}
    pelem= x.__getitem__(0)
    tourcom= x[:]+[pelem]
    
    primeros= tourcom[:8]
    ultimos= tourcom[1:]
    
    distancia=0
    for i,j in zip(primeros, ultimos):
        ruta= str(i)+'-'+str(j)
        rutaInv=str(j)+'-'+str(i)
        if  ruta in distanciaCiudades :
            distancia= distancia+ distanciaCiudades[ruta]
        elif rutaInv in distanciaCiudades :
            distancia= distancia+ distanciaCiudades[rutaInv]
        else:
            distancia=distancia+0
    return distancia

def listaCiudades(x):
    pelem= x.__getitem__(0)
    tourcom= x[:]+[pelem]
    return tourcom




"""Poblacion Inicla aleatoria, se le pasa un problema genetico y 
    el tamaño de la poblacion que quieres"""

def initial_population(problem_genetic,size):
    
    poblacion=[]
    gen=problem_genetic.genes
    
    for _ in range (0,size):
        random.shuffle(gen)
        
        poblacion=poblacion[:]+[gen[:]]
    return poblacion


"""Funcion que llama a crossover, para hacer el crossver de padres
    y devuelce los hijos"""
def crossover_parents(problem_genetic,parents):
    kids=[]
    for j in range(0,len(parents),2):
        kids.extend(problem_genetic.crossover(parents[j],parents[j+1]))
    return kids
"""Funcion que hace llama a mutation, para hacer la mutacion de individuos
    devuelve una lista con las mutaciones"""
def mutate_individuals1(problem_genetic, population, prob):
    populationMutation=[]
    for i in range(len(population)):
        populationMutation= populationMutation+ [problem_genetic.mutation(population[i],prob)] 
                     
    return populationMutation


"""###The function receives as input: an instance of
### Problem_Genetic, a population, a natural number n (number of
### individuals to be selected), a natural number k (number of
### participants in the tournament), and a function opt that can be
### either function max or function min (indicating if it is a
### maximization or a minimization problem)."""


def select_one_by_tournament(problem_genetic,population,k,opt):
    participants=random.sample(population,k)
    pop= opt(participants, key=problem_genetic.fitness)
    return pop
    
  

def tournament_selection(problem_genetic,population,n,k,opt):
    return [select_one_by_tournament(problem_genetic,population,k,opt)
             for _ in range(n)]
"""### The function receives as input:
##
### * problem_genetic: an instance of the class Problem_Genetic, with
###     the optimization problem that we want to solve.
### * k: number of participants on the selection tournaments.
### * opt: max or min, indicating if it is a maximization or a
###     minimization problem.
### * ngen: number of generations (halting condition)
### * size: number of individuals for each generation
### * ratio_cross: portion of the population which will be obtained by
###     means of crossovers. 
### * prob_mutate: probability that a gene mutation will take place."""

def genetic_algorithm_t(problem_genetic,k,opt,ngen,size,ratio_cross,prob_mutate):
    population= initial_population(problem_genetic,size)
    n_parents=int(round(size*ratio_cross))
    n_parents= (n_parents if n_parents%2==0 else n_parents-1)
    n_direct= size-n_parents

    for _ in range(ngen):
        population= new_generation_t(problem_genetic,k,opt,population,n_parents,n_direct,prob_mutate)

    best_chr= opt(population, key=problem_genetic.fitness)
    best=problem_genetic.decode(best_chr)
    return (best,problem_genetic.fitness(best_chr)) 

"""### new_generation_t(problem_genetic,k,opt,population,n_parents,n_direct,prob_mutate)
### that given a population, returns a new one (the next generation).."""

def new_generation_t(problem_genetic,k,opt,population,n_parents,n_direct,prob_mutate):
    parents = tournament_selection(problem_genetic,population,n_parents,k,opt)
    kids = crossover_parents(problem_genetic,parents)
    kids_m = mutate_individuals1(problem_genetic,kids,prob_mutate)
    direct = tournament_selection(problem_genetic,population,n_direct,k,opt)
    return direct + kids_m



prGen = Problem_Genetic([0,1,2,3,4,5,6,7],8,listaCiudades,lambda x: (decimal_to_distancia_ciudad(x)))
"""To test, uncomment one of the next lines"""
#genetic_algorithm_t(prGen,7,min,8,40,0.2,0.2)
#genetic_algorithm_t(prGen,25,min,18,40,0.7,0.4)




