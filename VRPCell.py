import random

"""Class gathering the necessary elements of the representation of
    optimization problems to be solved by a genetic algorithm. More
    precisely, these elements are:
   - genes: list of the genes used in the genotype of the individuals
   - individuals_length: length of the chromosomes
   - decode: function that transforms the genotype into the phenotype
   - fitness: function that evaluates the individuals, to be optimized
   - mutation: mutates a chromosome 
   - crossover: given a pair of chromosomes performs crossover on
    them"""
class Problem_Genetic(object):
   
    def __init__(self,genes,individuals_length,customerNumber,routesNum,almacen,decode,fitness):
        self.genes=genes
        self.individuals_length=individuals_length
        self.customerNumber=customerNumber
        self.routesNum=routesNum
        self.almacen=almacen
        self.decode=decode
        self.fitness=fitness
        
    def mutation(self,chrom,prob):
        cp_chrom= []
        cp_chrom.extend(chrom) 
        flag=True
        while flag:
            if random.random() < prob :
                aux1= random.randrange(len(chrom))
                aux2= random.randrange(len(chrom))
                if aux1 != aux2 :
                    val1= chrom[aux1]
                    val2= chrom[aux2]
                    cp_chrom[aux1]=val2
                    cp_chrom[aux2]=val1
                    flag= False
               
        return cp_chrom

    def crossover(self,c1,c2):
            
        pos=random.randrange(len(c1))               
        kid1=c1[:pos] 
        kid2=c2[:pos] 
            
        for i,j in zip(c1[pos:],c2[pos:]):
            if i not in kid2:
                kid2.append(i)
            if j not in kid1:
                kid1.append(j)
    
        if len(kid1)& len(kid2) is not (len(c1)):            
            for i,j in zip(c1,c2):
                if i not in kid2:
                    kid2.append(i)
                if j not in kid1:
                    kid1.append(j)
                      
               
        return [kid1,kid2]
    


"""Size-> Numero de ciudades del mapa
   CustomerNumber->Numero de clientes"""
def initial_population(problem_genetic,size,customerNum,routesNum):
 
    poblacion=[]
    gen= [x for x in problem_genetic.genes]
    if size%5 ==0:
        filas=size//5
    else:
        filas=(size//5)+1
    numPob=0
    for _ in range (filas):
        fila=[]
        for _ in range (5):
            if numPob!=size:
                random.shuffle(gen)
                fila.append(gen[:])
                numPob=numPob+1
            else:
                fila.append(0)
            
        poblacion.append(fila)
    
    return poblacion
    

"""Este metodo da la distancia del recorrido"""
def decimal_to_distancia_ciudad(x):
    distanciaCiudades={'0-1': 93, '0-2': 212, '0-3': 300, '0-4': 250, '0-5': 425, '0-6': 350, '0-7':590,
                       '1-2': 124, '1-3': 206, '1-4': 139, '1-5': 340, '1-6': 260, '1-7': 500,
                       '2-3': 236, '2-4': 263, '2-5': 361, '2-6': 454, '2-7': 523,
                       '3-4': 157, '3-5': 125, '3-6': 218, '3-7': 287,
                       '4-5': 202, '4-6': 117, '4-7': 364,
                       '5-6': 93, '5-7': 162,
                       '6-7': 255}
    capacidadCamiones={10:20,9:50,8:11}
    distanciaTotal=0
    numClientes=7
    rutas=[]
    order=[]
    camion=[10]
    for i in x:        
        if i <= numClientes:
            order.append(i)            
        elif i>numClientes:            
            rutas.append(order)
            camion.append(i)            
            order=[]
    rutas.append(order)   
        
    for i,c in zip(rutas,camion):
        productos=0
        distanciaRuta=0            
        primeros= [0]+i[:]
        ultimos= i[:]+[0]        
        for i,j in zip(primeros, ultimos):
            productos=productos+j
            ruta= str(i)+'-'+str(j)
            rutaInv=str(j)+'-'+str(i)
            if  ruta in distanciaCiudades :
                distanciaRuta= distanciaRuta+ distanciaCiudades[ruta]
            elif rutaInv in distanciaCiudades :
                distanciaRuta= distanciaRuta+ distanciaCiudades[rutaInv]
            else:
                distanciaRuta=distanciaRuta+0
        if productos>capacidadCamiones[c]:
            distanciaRuta=distanciaRuta*3
        distanciaTotal=distanciaTotal+distanciaRuta
         #"""Comprobar productos aqui"""   
    return distanciaTotal

def rutas(pop,numClientes,numRutas):
    rutas=[]
    order=[]
    
    numRuta=0
    for i in pop:        
        if i <= (numClientes):
            order.append(i)            
        elif i>numClientes:            
            rutas.append("Ruta "+str(numRuta)+":"+str(order))  
            numRuta=numRuta+1
            order=[]
    rutas.append("Ruta "+str(2)+":"+str(order))    
    return rutas

"""### The function receives as input:
##
### * problem_genetic: an instance of the class Problem_Genetic, with
###     the optimization problem that we want to solve.
### * opt: max or min, indicating if it is a maximization or a
###     minimization problem.
### * ngen: number of generations (halting condition)
### * size: number of individuals for each generation
### * prob_mutate: probability that a gene mutation will take place."""

def genetic_algorithm_t(problem_genetic,opt,ngen,size,prob_mutate):
    population= initial_population(problem_genetic,size,problem_genetic.customerNumber,problem_genetic.routesNum)
    
    for _ in range(ngen):
        population= new_generation_t(problem_genetic,opt,population,prob_mutate)
    best_InLine=[]
    for i in population:
        best_InLine.append(opt(i,key=problem_genetic.fitness))     
    best_chr= opt(best_InLine, key=problem_genetic.fitness)
    best=problem_genetic.decode(best_chr,problem_genetic.customerNumber,problem_genetic.routesNum)
    return (best,problem_genetic.fitness(best_chr)) 
"""### new_generation_t(problem_genetic,k,opt,population,n_parents,n_direct,prob_mutate)
### that given a population, returns a new one (the next generation).."""

def new_generation_t(problem_genetic,opt,population,prob_mutate):
    for x in range (len(population)):
        chroms=population[x]
        for y in range (len(chroms)):
            n_list= choose_neighborhood(population, x, y) 
            parents = choose_parents(problem_genetic,n_list)
            kids = crossover_parents(problem_genetic,parents)
            kids_m = mutate_individuals(problem_genetic,kids,prob_mutate)
            insert_if_better(problem_genetic,x,y,kids_m,population)    
    
    return population

def choose_neighborhood(population,x,y):
    neighbor=[]
    filas=len(population)-1
    columnas=len(population[x])-1
    if  population[x][y] !=0:
        neighbor.append(population[x][y])

    if x-1==-1:
        if population[filas][y]!=0:
            neighbor.append(population[filas][y]) 
    else:
        if  population[x-1][y] !=0:
            neighbor.append(population[x-1][y])           

    if x+1==filas+1:
        if population[0][y] !=0:
            neighbor.append(population[0][y])
    else:
        if  population[x+1][y] !=0:
            neighbor.append(population[x+1][y])   

    if y-1==-1:
        if  population[x][columnas] !=0:
            neighbor.append(population[x][columnas])
    else:
        if  population[x][y-1] !=0:
            neighbor.append(population[x][y-1])

    if y+1==columnas+1:
        if  population[x][0] !=0:
            neighbor.append(population[x][0])
    else:
        if  population[x][y+1] !=0:
            neighbor.append(population[x][y+1])
            
    return neighbor    

def choose_parents(problem_genetic,n_list):
    parents=[]
    rand= random.randrange(len(n_list))
    parents.append(n_list[rand])
    del n_list[rand]
    parents.append(min(n_list,key=problem_genetic.fitness))
    return parents

def crossover_parents(problem_genetic,parents):
    kids=[]
    kids.extend(problem_genetic.crossover(parents[0],parents[1]))
    return kids

def mutate_individuals(problem_genetic, kids, prob):
    populationMutation=[]
    for i in range(len(kids)):
        populationMutation.append(problem_genetic.mutation(kids[i],prob)) 
                     
    return populationMutation
def  insert_if_better(problem_genetic,x,y,betterKids,population):
    parents=[]
    parents.extend(betterKids)
    if population[x][y] != 0:
        parents.append(population[x][y])
         
    better_parent= min(parents,key=problem_genetic.fitness)
    population[x][y]=better_parent
    return population    
    
prGen = Problem_Genetic([0,1,2,3,4,5,6,7,8,9],21,7,3,4,rutas,lambda x: (decimal_to_distancia_ciudad(x)))
"""To test, uncomment one of the next lines"""
#genetic_algorithm_t(prGen,min,8,40,0.2)
